# README #

### What is this repository for? ###

Connecting to Haystack 3.0 via SCRAM mechanism.

### How do I get set up? ###

Below are the node commands that you would use to login a specified user. If trying to connect to a server running on https, you may need to set the fourth parameter to false which sets the rejectUnauthorized header in the https request.
    

```
#!javascript
E:\haystack-auth>node
> var demo = require('./index')
undefined
> var logMe = new demo.AuthClientContext("uri", "user", "pass", boolean)
undefined
> logMe.login(function (headers) {console.log("success: Authorization: " + headers["Authorization"]);}, function (msg) {console.log("Failed: " + msg);})
undefined
> success: Authorization: bearer authToken=YourToken
```


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact. 
* Consider posting questions on the [Project Haystack Forum](http://project-haystack.org/forum/topic)